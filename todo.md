# TODO

- [ ] AwesomeWM keybinds.

- [ ] AwesomeWM theme (including wallpaper, images, nav-bar images, etc).

- [ ] AwesomeWM nav-bar padding for widgets.

- [ ] Fix layouts that make the new opened client as the master. Atm they work
like LIFO, but they should work in FIFO.

- [ ] AwesomeWM custom resize mode similar to i3's.

- [ ] AwesomeWM network signal & nav-bar icon with hover tooltip (possibly with
keybind that pops the tooltip without hover).

- [ ] AwesomeWM enhanced daytime nav-bar widget with enhanced calendar popup:

  - [ ] Time on first row, date on the second row.

  - [ ] Calendar popup will have the ability to switch months by arrows, clicking
  on the current month to open a list of months, same thing for years, and a
  button to switch from month view to year view, the similar arrows as in
  the month view.

  - [ ] Using Google's Calendar API, parse for celebrations etc for each month.
  Save the celebrations and show them upon hover or some keybind press. This
  will also be the premise of creating custom events and saving them in the
  dotfiles location, as a json file.

~~- [ ] AwesomeWM sound signal and widget with hover tooltip, pie chart that fills,~~
~~and scroller when clicked. In the scroller popup mouse wheel should work.~~

~~- [ ] AwesomeWM update nav-bar icon and signal. Essentially when there are upgrades~~
~~available in pacman, show a (blinking till clicked??) update arrow.~~

- [ ] Above two widgets and many more are available [here](https://pavelmakhov.com/awesome-wm-widgets/).
Also more widgets, as well as other things, [here](https://github.com/atsepkov/awesome-awesome-wm)
and [here](https://github.com/Thomashighbaugh/Awesome-AwesomeWM-Modules-Widgets-And-Libraries).

- [ ] Custom lock screen where either everything is covered with a specific
colour or they are blured out. Maybe with a keypress feedback like i3-lock's?

- [ ] Rofi (theme, icons, everything).

- [ ] GTK theme to match everything (initially tokyonight-dark).

- [ ] Make a picon conf for VM and one for actual machine. The first one will use
xrender, no shadows and no fading, while the latter will use glx, shadows and
fading. ATM, the current one is for VM.

- [ ] NeoVim plugins.

- [ ] Flameshot installation and keybinds for it in AwesomeWM.

~~- [ ] Conky with both system and gpu info. Initially support only nvidia gpus.~~
~~or maybe use two terminals, one with some top alternative and one with watch~~
~~Nvidia-smi? (third optionally for better screen usage with nothing but onefetch~~
~~Or some graphic)~~

- [ ] Instead of Conky, run the appropriate commands and build widgets that will
show system usage, both for CPU and GPU (nvidia-smi has csv and more formats
for output).

- [ ] Tmux?
 
- [ ] Autostart apps with some being in specific tags (like in the example code
for FireFox).

- [ ] Pipewire.

- [ ] Light theme, AwesomeWM theme switch and indicator button, AwesomeWM or
cronjob to automatically change theme system-wide at specific times in the day.

- [ ] Zsh?

- [ ] .(bash,zsh)rc.

- [ ] Cmd aliases.

- [ ] Notifications should be saved in a notification panel. The panel would
open with keybinds, it would clear all notifications that are 1. not important,
and 2. over X hours before, where X is configurable. Appropriate icons are
available in nerd fonts under the term *message* and *notification*.

- [ ] Based on [TJ's video](https://www.youtube.com/watch?v=9gUatBHuXE0) it 
may be possible to create different run configurations similar to VS Code's.
Like a JSON where each configuration is run and then a keybind or something
to run the main configuration and one to select which one to run, maybe another
one for changing the main configuration and one for running the last run.
Embedded to the plugin, it should be possible to have a run active buffer file,
debug it, and so on. Note: this and the other videos for autocmd are better
suited for something like format on save (which again could be done easier
by a user cmd where :w is overriden to :lua vim.lsp.format()<CR>:w<CR> or
similar.
