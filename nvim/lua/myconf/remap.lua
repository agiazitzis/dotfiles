vim.g.mapleader = " "

-- Pressing space p and v opens Netrw.
vim.keymap.set("n", "<LEADER>pv", vim.cmd.Ex)

-- When in visual mode, pressing J (shift + j)
-- moves all selected lines down and pressing K
-- (shift + k) moves all selected lines up, while
-- also properly indenting them.
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Before remapping, J concatenated the current line
-- with the line below and moved the cursor at the end.
-- This makes the cursos stay in place.
vim.keymap.set("n", "J", "mzJ`z")

-- Before remapping, ctrl + d/u moved half a page down
-- or up and the cursor at the bottom of the page or 
-- at the top. This does the same but keeps the cursor
-- centered.
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- Allows jumping to next or previous match when searching
-- with the result staying in the middle.
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Allows for pasting over selection without lossing
-- the copy buffer content, by deleting the selection
-- to the void buffer.
vim.keymap.set("x", "<LEADER>p", "\"_dP")

-- The +y register is the system clipboard, so these allow
-- for copying to the system clipboard.
vim.keymap.set("n", "<LEADER>y", "\"+y")
vim.keymap.set("v", "<LEADER>y", "\"+y")
vim.keymap.set("n", "<LEADER>Y", "\"+Y")

-- Delete to void register.
vim.keymap.set("n", "<LEADER>d", "\"_d")
vim.keymap.set("v", "<LEADER>d", "\"_d")

-- When in shift + v (vertical edit mode) you have to
-- press escape in order for the changes to take effect.
-- This remap makes it so that ctrl + c also applies
-- the changes.
vim.keymap.set("i", "<C-c>", "<Esc>")

-- Format buffer using LSP.
vim.keymap.set("n", "<LEADER>f", function()
    vim.lsp.buf.format()
end)

-- Quickfix shortcuts.
vim.keymap.set("n", "<C-k>", "<cmd>cnext<CR>zz")
vim.keymap.set("n", "<C-j>", "<cmd>cprev<CR>zz")
vim.keymap.set("n", "<LEADER>k", "<cmd>lnext<CR>zz")
vim.keymap.set("n", "<LEADER>j", "<cmd>lprev<CR>zz")

-- Replace the word you are on.
vim.keymap.set("n", "<LEADER>h", ":%s/\\<<C-r><C-w>\\>//gI<Left><Left><Left>")
vim.keymap.set("v", "<LEADER>h", ":%s/\\<<C-r><C-w>\\>//gI<Left><Left><Left>")

-- Make a file executable without leaving neovim.
vim.keymap.set("n", "<LEADER>x", "<cmd>!chmod +x %<CR>", { silent = true })

-- Source current file
vim.keymap.set("n", "<LEADER><LEADER>", "<cmd>so<CR>")

