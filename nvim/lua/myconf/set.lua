vim.opt.guicursor = "" -- Whole letter cursor at all times.

vim.opt.nu = true -- Line numbers.
vim.opt.rnu = true -- Relative line numbers.

-- Four space indenting.
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true

-- No line wrapping.
vim.opt.wrap = false

-- Disable nvim backup.
vim.opt.swapfile = false
vim.opt.backup = false

-- Allow undotree to have access to all undos.
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- Disable highlighting except when searching.
vim.opt.hlsearch = false
-- Incremental search, good for guidance,
-- highlights as writting.
vim.opt.incsearch = true 

-- GUI colors on terminal.
vim.opt.termguicolors = true

-- No more or less lines before or after
-- cursor than the number specified,
-- except when at the top or bottom.
vim.opt.scrolloff = 8

-- Enables sign column, for files with defined
-- signs, such as debugging breakpoints, etc.
vim.opt.signcolumn = "yes"

-- Adds the character '@' to be included in
-- file and path names.
vim.opt.isfname:append("@-@")

-- Smaller update times.
vim.opt.updatetime = 50

-- Add column at 80 chars length.
vim.opt.colorcolumn = "80"








































