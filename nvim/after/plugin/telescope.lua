local builtin = require('telescope.builtin')

vim.keymap.set('n', '<leader>pf', builtin.find_files, {})
vim.keymap.set('n', '<C-p>', builtin.git_files, {})
vim.keymap.set('n', '<LEADER>ps', function()  -- requires ripgrep to be installed.
	builtin.grep_string({ search = vim.fn.input("Ripgrep > ") })
end)

