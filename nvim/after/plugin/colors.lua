function Coloring(color)
	color = color or "tokyonight-night"
	vim.cmd.colorscheme(color)
    vim.api.nvim_set_hl(0, "ColorColumn", { bg = require("tokyonight.colors").setup().fg }) -- custom color for colorcolumn, fg of theme
	vim.api.nvim_set_hl(0, "Normal", { bg = "none" }) -- removes bg color in Normal
	vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" }) -- removes bg color in Normal Floating window
end

Coloring()

