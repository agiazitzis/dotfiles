# Dotfiles

This repository houses the dotfiles I have created for my system. They are
tailored around my comfort. If you want to use them, clone the repo and modify
them to suit your needs.

## Installation

At the moment, there is no way to automatically install these, as they are in
development. In the future, a makefile will be provided to automatically
install them.

* Paru and ripgrep must be installed beforehand.

## TODO

[Here](./todo.md).

## License

Licensed under the [MIT License](./LICENSE).

